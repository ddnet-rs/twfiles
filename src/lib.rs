mod directories;

use std::fs::File;
use std::io;
use std::path::{Component, Path};

pub use directories::{cached_config_directory, cached_data_directory, Error};

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Version {
    DDNet06,
    Teeworlds07,
}

/// Verify that the path does not access some parent directory with `..`.
/// TODO: Might need to exclude mapres/../skins in the future.
/// Although twmap currently won't even permit those file paths.
fn check_no_parent_dir(path: &Path) -> Result<(), io::Error> {
    if path
        .components()
        .any(|component| component == Component::ParentDir)
    {
        Err(io::Error::new(
            io::ErrorKind::InvalidInput,
            "Attempt to access parent directory in Teeworlds/DDNet storage path",
        ))
    } else {
        Ok(())
    }
}

fn check_no_absoulte_path(path: &Path) -> Result<(), io::Error> {
    if path.is_absolute() {
        Err(io::Error::new(
            io::ErrorKind::InvalidInput,
            "Attempt to access an absolute path from Teeworlds/DDNet storage",
        ))
    } else {
        Ok(())
    }
}

/// Reads a file from the config or data directory.
/// `path` is a relative file path, e.g. `"mapres/grass_main.png"`.
pub fn read_file(path: &str, version: Version) -> Result<File, io::Error> {
    let path = Path::new(path);
    check_no_parent_dir(path)?;
    check_no_absoulte_path(path)?;
    if let Ok(cfg_dir) = cached_config_directory(version) {
        if let Ok(file) = File::open(cfg_dir.join(path)) {
            return Ok(file);
        }
    }
    match cached_data_directory(version) {
        Ok(data_dir) => File::open(data_dir.join(path)),
        Err(err) => Err(err.into()),
    }
}
