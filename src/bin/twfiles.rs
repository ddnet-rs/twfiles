use twstorage::{cached_config_directory, cached_data_directory, Version};

fn main() {
    for (version, game_name) in [
        (Version::DDNet06, "DDNet"),
        (Version::Teeworlds07, "Teeworlds"),
    ] {
        for (function, dir_name) in [
            (cached_data_directory as fn(_) -> _, "data"),
            (cached_config_directory as _, "config"),
        ] {
            if let Ok(dir) = function(version) {
                println!("{game_name} {dir_name} dir found: {dir:?}");
            } else {
                println!("No {game_name} {dir_name} dir detected.");
            }
        }
    }
}
