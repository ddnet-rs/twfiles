TwStorage
=========

Various tools require access to files distributed alongside the Teeworlds/DDNet client.
Additionally, the config directory of Teeworlds/DDNet needs to be taken into account for such file accesses.

This Rust crate locates the respective directories, and provides methods to access contained resources.

Future Work
-----------

Contributions are very welcome!
As of yet, this crate is still both bare-bones and untested.

TODOs:

- MacOS support
- More functionality (iterate over directories, create files, delete files)
- Create config directory, if unavailable
- More stable detection for Steam installations.
Does `~/.steam` always exist on Linux?
What about non-standard Steam installation locations on Windows?
Maybe parse (JSON) `libraryfolders.vdf` to detect all local Steam library paths.
